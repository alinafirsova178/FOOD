const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const path = require('path');

MongoClient.connect(url, function (err, client) {
	app.use(express.static('../frontend/static'))

	app.get('/salads', (req, res) => {
		res.sendFile(path.join(__dirname, '../frontend/salads-page.html'));
	})

	app.get('/basket', (req, res) => {
		console.log(__dirname)
		res.sendFile(path.join(__dirname, '../frontend/basket-page.html'));
	})

	app.listen(process.env.PORT || 9000)
})
